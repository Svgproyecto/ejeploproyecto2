# Diario de viajes

-   Se trata de una web donde los usuarios publican entradas sobre viajes.

-   Cada entrada tiene título, descripción, lugar y hasta 3 fotos asignadas.

-   Cada entrada puede ser votada con una puntuación entre 1 y 5.

## Endpoints del usuario ✅

-   POST - [/users] - Crea un usuario pendiente de activar. ✅
-   GET - [/users/validate/:registrationCode] - Valida un usuario recién registrado. ✅
-   POST - [/users/login] - Logea a un usuario retornando un token. ✅
-   GET - [/users/:idUser] - Retorna información de un usuario concreto. ✅
-   PUT - [/users/:idUser] - Edita el nombre o el email de un usuario.✅(pendiente de crear el verificar email, dia 20 minuto 2:16)
-   PUT - [/users/:idUser/password] - Edita la contraseña de un usuario.✅
-   PUT - [/users/:idUser/avatar] - Edita el avatar de un usuario.✅
-   PUT - [/users/password/recover] - Envia un correo con el código de reseteo de contraseña a un email.✅
-   PUT - [/users/password/reset] - Cambia la contraseña de un usuario con un código de reseteo.✅
-   DELETE - [/users/:idUser] - Borra un usuario.✅

## Endpoints del diario ✅

-   POST - [/entries] - Crea una entrada. ✅
-   GET - [/entries/:idEntry] - Retorna una entrada en concreto.✅
-   DELETE - [/entries/:idEntry] - Borra una entrada.
-   GET - [/entries] - Retorna el listado de entradas.
-   POST - [/entries/:idEntry/photos] - Añade una imagen a una entrada.
-   POST - [/entries/:idEntry/votes] - Vota una entrada. - DEBERES
-   PUT - [/entries/:idEntry] - Edita la descripción o el título de una entrada. - DEBERES
-   DELETE - [/entries/:idEntry/photos/:idPhoto] - Elimina una foto de una entrada.
