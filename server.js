//gracias a la dependecia dotenv y a requerirla aqui (en el fichero principal), puedo importar variables de entorno con el process.env, sin necesidad de requerirla en todos los ficheros que hacen referencia a este
require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const fileUpload = require('express-fileupload');

//creamos un servidor de express en la constante app
const app = express();

//extraemos el puerto de nuestro servidor de las variables de entorno 
const { PORT } = process.env;


/**
 * #################
 * ## Middlewares ##
 * #################
 */

const isAuth = require('./middleware/isAuth');
const canEditUser = require('./middleware/canEditUser');
const userExists = require('./middleware/userExists');

const entryExists = require('./middleware/entryExists')


/**
 * ###############################
 * ## Controladores de usuarios ##
 * ###############################
 */
//el metodo require de forma automatica va a buscar el fichero index.js si existe a no ser que le digamos otra cosa, por lo que no aria falta en este caso especificarlo
const { newUser, validateUser, loginUser, getUser, editUser, editUserPass, editUserAvatar, recoverUserPass, resetUserPass, deleteUser } = require('./controllers/users'); //const { newUser } = require('./controllers/users/index');


/**
 * ###############################
 * ## Controladores de entradas ##
 * ###############################
 */

const { getEntry, newEntry } = require('./controllers/entries')



//Middeleware que nos da informacion acerca de las peticiones que entran en el servidor (esto lo muestra en la terminal, para darnos informacion extra de lo que ocurre)
//habilitando asi la dependencia morgan
//informativa
app.use(morgan('dev'));

//Middleware que deserealiza el body en formato row (lo pasa de un formato Buffer a un formato JS) y lo pone disponible en la propiedad request.body
app.use(express.json());

//Middleware que desearila un body en formato 'form-data' y lo pone disponible en la propiedad 'req.body'
//si hay algun archivo en request.body estara disponible en la propiedad request.files. 
app.use(fileUpload());


//.................Vamos a crear todos los middlewares que tienen nuestra pagina...........................

/**
 * ###########################
 * ## Endopoins de usuarios ##
 * ###########################
 */

//Crear un nuevo usuario, 
    //como primer argumento la ruta que hayamos decidido y como segundo argumento la funcion callback que se ejecutara cuando un usuario se reguistre
    //esta funcion de callback para tener el codigo mas ordenado la hacemos en un fichero aparte y la importamos posteriormente a este, la carpeta donde se encontraran decidimos que sera controllers
app.post('/users', newUser)

//Validar un nuevo usuario
app.get('/users/validate/:registrationCode', validateUser)

//Login User
app.post('/users/login', loginUser)

//Obteenr informacion de un usuario
app.get('/users/:idUser', isAuth, userExists, getUser)

//Editar perfil
app.put('/users/:idUser', isAuth, userExists, canEditUser, editUser)

//Editar la password del un usuario
app.put('/users/:idUser/password', isAuth, userExists, canEditUser, editUserPass)

//Editar el avatar del usuario 
app.put('/users/:idUser/avatar', isAuth, userExists, canEditUser, editUserAvatar)

//Enviar un codigo de recuperacion de la password al email del usuario
app.put('/users/password/recover', recoverUserPass);

//Resetea la password de un usuario utilizando el codigo de recuperacion 
app.put('/users/password/reset', resetUserPass);

//Anonimizar un usuario 
app.delete('/users/:idUser', isAuth, userExists, canEditUser, deleteUser);
//cuidado porque para que un usuario con conocimientos una vez 'eliminado' no pueda subir entradas o editar su perfil en el middleware de isAuth, fabricamos que si el usuario no esta activo o esta eliminado, lanzasemos un error, de esa manera el token de un usuario eliminado no va a ser valido




/**
 * ###########################
 * ## Endopoins de entradas ##
 * ###########################
 */

//Creamos una nueva entrada, (en el postman como vamos a enviar imagenes y texto a la vez, utilizamos directamente form-data) esto sucede porque tenemos un middleware arriba del todo (expres.fileUpload que lo tenemso como app.use(fileUpload()), que se encarga de gestionar los datos que le llegan  )
app.post('/entries', isAuth, newEntry)

//Obtenemos informacion de la entrada, no hace falta estar logeado porque nosotros lo hemos decidido asi 
app.get('/entries/:idEntry', entryExists, getEntry); //en este endpoin especificamso aqui (50), la entrada en la que clicamos, de la que queremos obtenner la informacion



//............................................ 

//Intentamos entrar en el middleware de error, si no fuese posible entrariamos en el de no encontrado, el orden es imoportante
//Middleware de error.
//estamos obligados a requerir los 4 paramertos para que express pueda identificarlo correctamente como el middleware de error, asi el next(error), entrara aqui inequivocamente ya que es el unico middleware con el parametro error
            // eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
    console.log(error);//esto unicamente es para que en consola se nos muestre un error algo mas claro del error 
    res.status(error.httpStatus || 500).send({
        status: 'error', 
        message: error.message
    });
});


//Middeleware de no encontrado
    //si existe un error lo imprimimos para el cliente en modo de respuesta (res)
app.use((req, res) => {
    res.status(404).send({
        status: 'error', 
        message: 'Not found'
    });
});


//Ponemos el servidor a escuchar un puerto 
app.listen(PORT, () => {
    console.log(`Server llistening http://localhost${PORT}`);
});