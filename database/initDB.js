//ESTE FICHERO NUNCA SE VA A SUBIR (ES UN FICHERO PARA GENERAR UNA BASE DE DATOS), no acabo de pillar pues como funciona esto

const getDB = require('./getDB');
const faker = require('faker/locale/es');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const { format } = require('date-fns');

// Función que formatea una fecha a DATETIME.
function formatDate(date) {
    return format(date, 'yyyy-MM-dd HH:mm:ss');
}

//Funcion que genera un numero aleatorio entre 2 valores un minimo y un maximo (la utilizamos para un apartado de generar entradas faker)
function getRandom(min, max){
    return Math.round(Math.random() * (max - min) + min);
}

async function initDB() {
    // Creamos una variable que almacenará una futura conexión.
    let connection;

    try {
        // Obtenemos una conexión libre con la base de datos.
        connection = await getDB();

        // Eliminamos las tablas existentes.
        await connection.query('DROP TABLE IF EXISTS entry_votes');
        await connection.query('DROP TABLE IF EXISTS entry_photos');
        await connection.query('DROP TABLE IF EXISTS entries');
        await connection.query('DROP TABLE IF EXISTS users');

        console.log('Tablas eliminadas');

        // Crear tabla de usuarios.
        await connection.query(`
            CREATE TABLE users (
                id INT PRIMARY KEY AUTO_INCREMENT,
                email VARCHAR(100) UNIQUE NOT NULL,
                password VARCHAR(100) NOT NULL,
                username VARCHAR(50),
                avatar VARCHAR(50),
                active BOOLEAN DEFAULT false,
                deleted BOOLEAN DEFAULT false,
                role ENUM("admin", "normal") DEFAULT "normal" NOT NULL,
                registrationCode VARCHAR(100),
                recoverCode VARCHAR(100),
                createdAt DATETIME NOT NULL,
                modifiedAt DATETIME
            )
        `);

        // Crear la tabla de entradas.
        await connection.query(`
            CREATE TABLE entries (
                id INT PRIMARY KEY AUTO_INCREMENT,
                title VARCHAR(100) NOT NULL,
                description TEXT,
                idUser INT NOT NULL,
                FOREIGN KEY (idUser) REFERENCES users(id),
                createdAt DATETIME NOT NULL,
                modifiedAt DATETIME
            )
        `);

        // Crear la tabla de fotos.
        await connection.query(`
            CREATE TABLE entry_photos (
                id INT PRIMARY KEY AUTO_INCREMENT,
                name VARCHAR(50) NOT NULL,
                idEntry INT NOT NULL,
                FOREIGN KEY (idEntry) REFERENCES entries(id) ON DELETE CASCADE,
                createdAt DATETIME NOT NULL
            )
        `);

        // Crear la tabla de votos.
        await connection.query(`
            CREATE TABLE entry_votes (
                id INT PRIMARY KEY AUTO_INCREMENT,
                vote TINYINT NOT NULL,
                idEntry INT NOT NULL,
                FOREIGN KEY (idEntry) REFERENCES entries(id) ON DELETE CASCADE,
                idUser INT NOT NULL,
                FOREIGN KEY (idUser) REFERENCES users(id),
                createdAt DATETIME NOT NULL
            )
        `);

        console.log('Tablas creadas');

        // Creamos la contraseña del administrador y la encriptamos.
        const ADMIN_PASS = await bcrypt.hash('123456', saltRounds);

        // Insertamos el usuario administrador.
        await connection.query(`
            INSERT INTO users (email, password, username, active, role, createdAt)
            VALUES (
                "sspamvg@gmail.com",
                "${ADMIN_PASS}",
                "Admin",
                true,
                "admin",
                "${formatDate(new Date())}"
            )
        `);

        // Constante que nos dice el nº de usuarios que vamos a crear.
        const USERS = 10;

        // Creamos un bucle que se repite tantas veces como nº de usuarios.
        for (let i = 0; i < USERS; i++) {
            // Datos faker.
            const email = faker.internet.email();
            const username = faker.name.findName();
            const password = await bcrypt.hash('123456', saltRounds);

            //insertamos un usuario en cada repeticion
            await connection.query(`
                INSERT INTO users (email, username, password, active, createdAt)
                VALUES (
                    "${email}", 
                    "${username}",
                    "${password}",
                    true, 
                    "${formatDate(new Date())}"
                )
            `);
        }
        console.log('Usuarios creados');
  
        // Constante que nos dice el nº de usuarios que vamos a crear.
        const ENTRIES = 100;

        // Creamos un bucle que se repite tantas veces como nº de usuarios.
        for (let i = 0; i < ENTRIES; i++){
            //Datos fake
            const title = faker.address.city();
            const description = faker.lorem.paragraph();
            const idUser = getRandom(2, USERS + 1);

            //insertamos las entradas
            await connection.query(`
                INSERT INTO entries (title, description, idUser, createdAt)
                VALUES (
                    "${title}",
                    "${description}",
                    "${idUser}",
                    "${formatDate(new Date())}"
                )
            `)
        }
        console.log('Entradas creadas');
    } catch (err) {
        console.error(err);
    } finally {
        // Si existe una conexión la liberamos.
        if (connection) connection.release();

        // Cerramos el proceso actual.
        process.exit();
    }
}

// Llamamos a la función anterior.
initDB();
