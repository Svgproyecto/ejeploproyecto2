require('dotenv').config();

const mysql = require('mysql2/promise');

const { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;

// Variable que almacenará un pool de conexiones.
let pool;

// Función que retorna una conexión a la base de datos.
const getDB = async () => {
    // Si no hay conexión...
    if (!pool) {
        // Creamos un grupo de conexiones.
        pool = mysql.createPool({
            connectionLimit: 10,
            host: MYSQL_HOST,
            user: MYSQL_USER,
            password: MYSQL_PASSWORD,
            database: MYSQL_DATABASE,
            timezone: 'Z',
        });
    }

    // Retornamos una conexión libre.
    return await pool.getConnection();
};

// Exportamos la función.
module.exports = getDB;
