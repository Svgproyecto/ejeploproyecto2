const getDB = require("../database/getDB");

const userExists = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //obtenemos el id del usuario al que queremos acceder 
        const {idUser} = req.params;

        //obtenemos el usuario, si esta eliminado no deberiamos poder seleccionarlo
        const [users] = await connection.query(
            `SELECT id FROM users WHERE id = ? AND deleted = false`,
            [idUser]
        );

        //si el usuario no existe lanzamos un error 
        if(users.length < 1){
            const error = new Error('El usuario no existe');
            error.httpStatus = 404;
            throw new error;
        }

        //si todo va bien y no saltamos a un error, pasamos el control a la siguiente funcion 
        next();
    } catch (error) {
        next(error)
    }finally{
        if(connection) connection.release();
    }
}

module.exports = userExists;