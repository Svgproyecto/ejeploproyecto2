const getDB = require('../database/getDB');

const canEditUser = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //Obtenemos el id del usuario que queremos editar
        const {idUser} = req.params; 

        //si el usuario que intenta editar no es el mismo que el que realiza la peticion lanzamos un error 
        //(estas comprobaciones son de seguridad por si un usuario con conocimientos intentase hacer cosas raras, ya que en realidad al propio usuario es al unico al que le apareceria este campo de edicion)
        //cuidado idUser viene como string por lo que al compararlo con idReqUser que viene como numero daria error, por eso pasamos  a numero el primero 
        //lanzamos un error si el id del usuario que queremos editar y el id del usuario que realiza la request(peticion) no es el mismo 
        if(Number(idUser) !== req.userAuth.id){
            const error = new Error('No tienes suficientes permisos');
            error.httpStatus = 403;
            throw error;
        }

        //si todo a ido bien damos paso a la sigiente funcion 
        next();


    } catch (error) {
        next(error);
    }finally{
        if (connection) connection.release();
    }
};

module.exports = canEditUser;