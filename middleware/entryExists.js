const getDB = require('../database/getDB');

const entryExists = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //obtenemso el id de la entrada que queremso obtener
        const {idEntry} = req.params

        //Tratamos de obtener la entrada de la base de datos 
        const [entries] = await connection.query(
            `SELECT id FROM entries WHERE id =?`,
            [idEntry]
        );

        //si la entrada no existe lanzamos un error 
        if(entries.length < 1){
            const error = new Error('La entrada no existe');
            error.httpStatus = 404;
            throw error;
        }

        //si todo a bien pasamos el control a la siguiente funcion 
        next();


    } catch (error) {
        next(error)
    }finally{
        if(connection) connection.release()
    }
}

module.exports = entryExists