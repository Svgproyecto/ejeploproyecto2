//para crear y enviar codigo de registro
const crypto = require('crypto'); //esta dependencia ya viene en el core modules de node, no hace falta instalarla
const sgMail = require('@sendgrid/mail');

//para subir ficheros 
const path = require('path') //necesitamos una ruta absoluta al directorio donde se van a guardar las imagenes 
const sharp = require('sharp') //dependencia que nos permite trabajar con imagenes (editar tamaho filtros, etc)
const uuid = require('uuid') //nos permite generar un id alfanumerico aleatorio, otra opcion aparte de crypto , en este caso lo vamos a utilizar para generar nombres aleatorios para las fotos
const {ensureDir, unlink} = require('fs-extra')//funciones de fs(que ya esta en node), pero mejoradas
//meidante destructurin de fs-extra obtenemos ensureDir(comprueba que el directorio que le pasamos existe y si no existe lo crea) y unlink(que me permite eliminar un archivo del servidor)

//importamos las variables de entorno
const { SENDGRID_API_KEY, SENDGRID_FROM, UPLOADS_DIRECTORY } = process.env;

//Asignamos la API KEY de Sendgird
sgMail.setApiKey(SENDGRID_API_KEY);

//Creamos la ruta absoluta al directorio donde  se subiran los archivos(fotos en este caso) 
const uploadDir = path.join(__dirname, UPLOADS_DIRECTORY) //static a la que hacemos referencia en el fichero .env es el directorio general donde tenemos ficheros estaticos que usa nuestro servidor, en este cso creamos una carpeta dentro que se llamara uploads


/**
 * #####################################
 * ## Generar una cadena alfanumerica ##
 * #####################################
 */

function generateRandomString (length) {
    return crypto.randomBytes(length).toString('hex');
}

/**
 * #############################################
 * ## Enviar un Email de activacion de cuenta ##
 * #############################################
 */

async function sendMail({to, subject, body}) {
    try {
        //preparamso el mensaje que se enviara
        const msg = {
            to, //para
            from: SENDGRID_FROM, //de
            subject, //asunto
            text: body, //cuarpo del mensaje
            html: //composicon del mail
            ` 
                <div>
                    <h1>${subject}</h1>
                    <p>${body}</p>
                </div>
            `
        };

        //enviamos el mensaje
        await sgMail.send(msg);

    } catch (error) {
        console.error(error);
        throw new Error('Hubo un problema al enviar el mensaje');
    }
}

/**
 * ####################################
 * ## Guardar una foto en el servidor##
 * ####################################
 */
async function savePhoto(image, type) { //existen dos tipos de fotos en nuestra api, las de las entradas y las de avatar, y tiene especificaciones diferentes de tamaho porejemplo, en nuestro caso dera 0 para avatar y 1 para fotos de entradas 
    try {
        //Comprobamos que el directorio de subida de imagenes exista 
        await ensureDir(uploadDir);

        //convertimos la imagen en un objeto sharp 
        //La image que nosotros le vamos a pasar (req.files.avatar), es un objeto con muchas propiedades, entre ellas esta la propiedad data con formato bufer, esa es la que le queremos pasar, sharp se encagara de generar otro objeto sharp de tipo bufer con mas informacion acerca de la imagen y asi poder editarla 
        //req.files.data {
        //   ...
        //    data:<Buffer>
        //}
        const sharpImage = await sharp(image.data);

        //Accedemos a los metadatos de la imagen para poder comprobar el ancho total a traves del metodo metadata de sharp 
        const infoImage = await sharpImage.metadata();

        //si el type de imagen es 0 (avatar), sharp redimensiona la imagen a 150 x 150
        if(type === 0) {
            sharpImage.resize(150, 150)//metodo de sharp para redimensionar la imagen, matiene las proporciones pero recorta la imagen 
        }

        //si la imagen es de type 1 (entrada) y la imagen supera el ancho indicado, redimensionamos la imagen 
        else if(type === 1 && infoImage.width > 1000) {
            sharpImage.resize(1000); //si le pasamos un argumento, solo hace referencia al ancho y mantiene las propociones
        }

        //Generamos un nombre unico para la imagen
        const imageName = `${uuid.v4()}.jpg`;

        //creamos una ruta absoluta a la ubicacion donde queremos guardar la imagen 
        const imagePath = path.join(uploadDir, imageName);

        //guardamos la imagen en el directorio upload, los objetos de type sharp tienen un metodo que nos permite gaurdar laa imagen (archivos)(?realmente es un nombre de imagen creo?) en el directorio que queramos, sin necesidad de recurrir a fs(con el metodo writefile)
        await sharpImage.toFile(imagePath);

        //retornamos el nombre de la imagen
        return imageName;
    
    } catch (error) {
        console.error(error);
        throw new Error('Error al procesar la imagen')
    }
}

/**
 * ####################################
 * ## Eliminar una foto del servidor##
 * ####################################
 */
async function deletePhoto (photoName) {
    try{
        //Creamos la ruta absoluta a la foto 
        const photoPath = path.join(uploadDir, photoName);

        //Eliminamos la foto del disco
        await unlink(photoPath)
    }catch(error){
        console.error(error);
        throw new Error('error al eliminar la imagen del servidor')
    }
}


module.exports = {
    generateRandomString, 
    sendMail, 
    savePhoto, 
    deletePhoto
};

