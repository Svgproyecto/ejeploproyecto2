const getDB = require("../../database/getDB");

const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");


const loginUser = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //Obtenemos el email y la password
        const {email, password} = req.body;

        //si falta algun campo lanzamos un error 
        if(!email || !password) {
            const error = new Error('Faltan campos');
            error.httpStatus = 400;
            throw error;
        }

        //comprobamos si existe algun usuario con ese email
        const [users] = await connection.query(
            `SELECT id, role, password, active FROM users WHERE email = ?`,
            [email]
        );
        
        //Variable que almacena un valor Booleano
        let validPassword;
        //si existe un usuario con el email establecido pasamos a la siguiente comprobacion
        if(users.length > 0) {
            //comprobamos si la password es correcta o incorrecta
                                                    //comprobamos si la password introdicida por el usuario es igual que la guardada en la base de datos, que en este caso se encuentra en la variable users que hemos sacado anteriromente mediante destructurind de le array de arrays, y ese usuario se encontrara en la primera posicion nuevamente del segundo array (que ya tenemos en la variable users), a su vez ese array contiene un objeto con la propiedad password entre otros, que es lo que ahora deseamos obtener
            validPassword = await bcrypt.compare(password, users[0].password);
        }

        //Si no hay ningun usuario con este email o la password no es correcta lanzamos un error
        if (users.length < 1 || !validPassword){
            const error = new Error('Email o password incorrectos');
            error.httpStatus = 401;
            throw error;
        }

        //Si el usuario existe perono esta activo, lanzamos un error
        if (!users[0].active){
            const error = new Error('Revisa tu correo para activar tu cuenta');
            error.httpStatus = 401;
            throw error;
        }

        //Crear un token
            //Objeto con los datos que quiero que contenga el token (por defecto tiene la fecha en la que se creo y cuando caduca, por lo general 1000 segundos)
        const tokenInfo = {
            id: users[0].id, //necesitamos saber el id para saber que usuario es, y  mas adelante nos hara falta esta informacion como por ejemplo para subir una entrada necesitaamos saber que usuario es, esta informacion la sacaremos del token que estata en el header autorization
            role: users[0].role, // lo mismo con el role, necesitamos saber que permisos tiene
        }
            //Creamos el token
        const token = jwt.sign( //el metodo sing de jwt necesita 3 argumentos 
            tokenInfo, //objeto con las propiedades que queramos agregar
            process.env.SECRET,//Clave alfanumerica (aporreamiento de teclado)
            {expiresIn: '50d'}//Fecha de expioracion en formato objeto, lo ideal es que el token caduque cada poco tiempo, como ahora estamos haciendo un ejemplo de proyecto vamos a poner mucho para no ir actualizando el postan cada x horas
        );

        //le enviamos al usuario el token
        //por ser curiosos si al usuario le enviamos un mensaje ponemos directamente al propiedad message en el objeto dentro de send, pero si enviamos informacion add una propiedad data, objeto, con la informacion a enviar
        res.send({
            status: 'ok',
            data: {
                token
            }
        });

    } catch (error) {
        next(error);
    }finally{
        if(connection) connection.release();
    }
}


module.exports = loginUser;


//SENDGRID_API_KEY=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