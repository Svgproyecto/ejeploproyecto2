const getDB = require('../../database/getDB');

const getUser = async (req, res, next) => {
    //Antes de ejecutar esta funcion controladora debemos saber si es usuario esta logeado o no (gracias al middleware de isAuth que enlazaremos en el fichero server antes de ejecutar esta funcoion que tambien exportaremos a fichero server)
    let connection;

    try {
        connection = await getDB();
        //obtenermos el id del usuario del que queremos obtener la info
        const {idUser} = req.params; //params son los parametros con : antes del nombre de la ruta de nuestro servidor
        

        //obtenemos el id del usuario que intenta realizar la request
            //como esta funcion se ejecuta despues de isAuth porque asi vamos ha hacer que ocurra en el fichero server, damos por echo que conocesmos el valor de req.userAuth (que en nuestro caso tiene dos propiedades id y role) creado en el fichero isAuth 
        const idReqUser = req.userAuth.id;

        //Obtenemos todos los datos que me interesan del usuario del cual se solicita la informacion
        const [users] = await connection.query(
            `SELECT id, username, email, avatar, role, createdAt FROM users WHERE id = ?`,
            [idUser]
        );
        
        //si no hay usuarios con ese id lanzamos un error 
        /*esto no haria falta porque le pasamos en el fichero server el middleware de userExist por lo que si no existe no llegaremos a este fichero, no lo elimino por dejar evidencia de ello y asi mejorar  
        if (users.length < 1) {
            const error = new Error('Usuario no encontrado');
            error.httpStatus = 404;
            throw error;
        } */

        //si existe creamos un objeto con la informacion basica del usuario, que todos podran ver
        const userInfo = {
            username: users[0].username,
            avatar: users[0].avatar,
        }

        //si el usuario que solicita los datos es el usuario dueho o un administrador ahadimos informacion extra
        if(users[0].id === idReqUser || req.userAuth.role === 'admin') {
            userInfo.email = users[0].email;
            userInfo.role = users[0].role;
            userInfo.createdAt = users[0].createdAt;
        }
        res.send({
            status: 'ok',
            data: {
                user: userInfo
            }
        });


    } catch (error) {
        next(error)
    }finally{
        if(connection) connection.release();
    }
};

module.exports = getUser;