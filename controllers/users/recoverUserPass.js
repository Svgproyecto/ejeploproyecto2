const getDB = require('../../database/getDB');

//como vamos a enviarle un correo al usuario necesitamos 2 funciones del fichero helpers.
const { generateRandomString, sendMail } = require('../../helpers')

const recoverUserPass = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //Obtenemos el email del usuario 
        const { email } = req.body;
        //si falta el email lanzamos un error
        if(!email) {
            const error = new Error('Faltan campos');
            error.httpStatus = 400;
            throw error
        }

        //comprobamos si el email existe en la base de datos 
        const [users] = await connection.query(
            `SELECT id FROM users WHERE email = ?`,
            [email]
        );

        //si existe un mensaje con este email enviamos un codigo de recuperacion, pero ojo si no existe este email en este caso no lanzaremos un error, porque no quiero dar ninguna pista a un supuesto usuario maligno de que este email exista o no 
            if(users.length > 0) {
                //generamos un codigo de recuperacion
                const recoverCode = generateRandomString(20);

                //creamos el body del mensaje
                const emailBody = `
                    Se solicito un cambio de constraseña para el usuario registrado con este email, en la app ejemplo Proyecto 2.

                    El codigo de recuperacion es: ${recoverCode}

                    Si no has sido tu ignora este email.
                `;

                //enviamos el email
                await sendMail({
                    to: email,
                    subject: 'Cambio de contraseña en ejemplo Proyecto 2',
                    body: emailBody
                });

                //Agregamos el codigo de recuperacion al usuario con dicho email
                await connection.query(
                    `UPDATE users SET recoverCode = ?, modifiedAt = ? WHERE email = ?`,
                    [recoverCode, new Date(), email]
                )

            }

            //Para no dar pistas de si existe o no existe el email que estamos intentando actualizar como hemos mencionado anteriormente, enviamos la respuesta al usuario simpre tanto si existe como si no existe el email y por eso lo ponemos fuera del if de si existe el email, simpre enviaremos la respuesta
            res.send({
                status: 'ok',
                message: `Si el email "${email}" existe se a enviado un codigo de recuperacion`
            })

    } catch (error) {
        next(error);
    }finally{
        if(connection) connection.release();
    }
}

module.exports = recoverUserPass;
