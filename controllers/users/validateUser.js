const getDB = require("../../database/getDB");

const validateUser = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //Obtenemos el codigo de registro
        const { registrationCode } = req.params; //propiedad donde se encuentran los parametros

        //Comprobamos si existe algun usuario pendiente de validar con ese codigo anterior
        //este users no es mas que el nombre que decido darle meidante destructuring al primer parametro del arrays de arrays que estoy obteniendo 
        const [users] = await connection.query(
            `SELECT id FROM users WHERE registrationCode = ?`, 
            [registrationCode]
        )


        /* mini aclaracion de users y el codigo de registro con la comprobacion pertinente 
        users = [{ id : 2 }], 
        no va a ver mas de dos usuarios pendientes de registrarse con el mismo codigo , las posibilidades son incimas y ademas al registrarse elimino ese codigo del usuario */
        
        
        //Si no hay usuarios pendientes de validar con ese codigo de registro lanzamos un error
        if(users.length < 1) {
            const error = new Error('No hay usuarios pendientes de validar con ese codigo de registro')
            error.httpStatus = 404;
            throw error
        }

        //sino se ha activado el error del if anterior significa que queda algun usuario pendiente de validar
        //Activamos el usuario y eliminamos el codigo de registro
        await connection.query(
            `UPDATE users SET active = true, registrationCode = NULL WHERE registrationCode = ?`,
            [registrationCode]
        );

        res.send({
            status:'ok',
            message: 'Usuario activado'
        })

    } catch (error) {
        next(error)
    }finally{
        if (connection) connection.release();
    }
}


module.exports = validateUser;