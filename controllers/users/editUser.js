const getDB = require('../../database/getDB');



const editUser = async (req, res, next) => {
    let connection;

    try{
        connection = await getDB();

        //comprobamos que el usuario que quiere editar el perfil es el mismo usuario dueho de perfil
        
        //Obtenemos el id del usuario que queremos editar 
        const {idUser} = req.params;

        //Obtenemos los campos editables del body 
        const {username, newEmail} = req.body

        //si faltan los dos campos a editar lanzamos un error
        if(!username && !newEmail) {
            const error = new Error('Faltan campos');
            error.httpStatus = 400;
            throw error;
        }

        

    //vamos a crear como middlewares si el usuario puede ser editado y si el usuario existe, y lo implementaremos directamete en el fichero server.js antes de esta funcion, asi si esos middlewlares se cumplen todo ira bien 



        //Llegados a este punto se supone que el usuario es el mismo por lo que ya podemos obtener los campos a editar
        //Obtenemos el email y el username del usuario
        const [users] = await connection.query(
            `SELECT email, username FROM users WHERE id = ?`,
            [idUser]
        );


        /**
         * ###########
         * ## Email ##
         * ###########
         */

        //comprobamos si el nuevo email es distinto al anterior y si existe en nuevo email 
        if(newEmail && newEmail !== users[0].email) {
            //comprobamos que no exista en la base de datos
            const [usersEmail] = await connection.query(
                `SELECT id FROM users WHERE email = ?`,
                [newEmail]
            );

            if (usersEmail.length > 0){
                const error = new Error('Ya existe un usuario con ese email');
                error.httpStatus = 409;
                throw error;
            }

            //Si no salta ningun error actualizamos el usuario en la base de datos 
            await connection.query (
                `UPDATE users SET email = ?, modifiedAt = ? WHERE id = ?`, 
                [newEmail, new Date(), idUser]
            )

        }


        

        /**
     * ##############
     * ## Username ##
     * ##############
     */

        if(username && username !== users[0].username){

            //comprobamos que no exista en la base de datos 
            const [usersName] = await connection.query(
                `SELECT id FROM users WHERE username = ?`,
                [username]
            )
            if(usersName.length > 0) {
                const error = new Error('Ya existe un usuario con ese nombre');
                error.httpStatus = 409;
                throw error;
            }
                
            //si no salta ningun error actualizamos la base de datos 
            await connection.query(
                `UPDATE users SET username = ?, modifiedAt = ? WHERE id = ?`,
                [username, new Date(), idUser]
            )
        }

        //enviamos una respuesta de si todo a ido bien  
        res.send({
            status: 'ok',
            message: 'Usuario actualizado con exito, si has editado tu email debes verfificarlo en el correo que te hemos enviado'
        })

    }catch(error){
        next(error);
    }finally{
        if(connection) connection.release()
    }
}

module.exports = editUser;