const getDB = require('../../database/getDB');

//como se va a generar una nueva contraseña, debemos encryptarla y nosotros hemos decidido usar la dependencia bcrypt, la requerimos pues
const bcrypt = require('bcrypt');
const saltRounds = 10;

const resetUserPass = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //requerimos y obtenemos el codigo de recuperacion y la nueva contraseña
        const { recoverCode, newPassword } = req.body;

        //Si falta algun campo lanzaos un error
        if(!(recoverCode || newPassword)) {
            const error = new Error('Faltan campos');
            error.httpStatus = 400;
            throw error;
        }

        //si todo va bien obtenemos el codigo de recuperacion y de manera algo indirecta el id del usuario que tiene ese codigo de recuperacion, lo usaremos mas adelante
        const [users] = await connection.query(
            `SELECT id FROM users WHERE recoverCode = ?`,
            [recoverCode]
        );
        //si no existe ningun usuario con ese codigo de recuperaion, lanzamos un error
        if(users.length < 1){
            const error = new Error('Codigo de recuperacion incorrecto');
            error.httpStatus = 404;
            throw error;
        }

        //si llegamos aqui es que todo va bien y no a saltado ningun error
        //haseamos la nueva contraseña
        const hashedPassword = await bcrypt.hash(newPassword, saltRounds);

        //Actualizamos la contraseña del usuario (que localizamos mediente su supuesto recoverCode unico)
        await connection.query(
            `UPDATE users SET password = ?, recoverCode = NULL, modifiedAt = ? WHERE id = ?`,
            [hashedPassword, new Date(), users[0].id]
        );

        res.send({
            status:'ok',
            message:'Contraseña actualizada'
        })
    } catch (error) {
        next(error);
    }finally{
        if(connection) connection.release();
    }
}

module.exports = resetUserPass;
