const getDB = require("../../database/getDB");
//como vamos a modificar la password del usuario necesitamos la dependencia bcrypt
const bcrypt = require('bcrypt');
const saltRounds = 10;

//no nos vamos a parar si tenemos permiso para editar o no y aque el middleware de canEditUser lo va a hacer por nosotros cuando le llamemos antes de esta funcion el el fichero server
const editUserPass = async(req, res, next) =>{
    let connection;

    try {
        connection = await getDB();

        //Obtenemos el id del usuario que queremos editar (en el que hacemos clic...)
        const { idUser } = req.params;

        //Obtenemos la password vieja y la nueva
        const  {oldPassword, newPassword} = req.body;
         
        //Obtenemos la password del usuario 
        const [users] = await connection.query(
            `SELECT password FROM users WHERE id = ?`,
            [idUser]
        );

        //guardamos en una variable un valor booleano: contraseha correcta o incorrecta
        //comparando la contraseha vieja que hemos indicado en el body con la contraseha vija guardada en la base de datos, esto lo hacemos con el metodo compare de bcypt
        const isValid = await bcrypt.compare(oldPassword, users[0].password);
        
        //si la password no es valida lanzamos un error 
        if(!isValid) {
            const error = new Error('La contraseha no coincide')
            error.httpStatus = 401;
            throw error
        }

        //si tod va bien cambiamos la contraseha, primero hasheamos la nueva contraseha
        const hashedPassword = await bcrypt.hash(newPassword, saltRounds);

        //Actualizamos la base de datos 
        await connection.query(
            `UPDATE users SET password = ?, modifiedAt = ? WHERE id = ?`,
            [hashedPassword, new Date(), idUser]
        );

        res.send({
            status:'ok',
            message: 'Password actualizada'
        });


    } catch (error) {
        next(error);
    }finally{
        if(connection) connection.release()
    }
}

module.exports = editUserPass;