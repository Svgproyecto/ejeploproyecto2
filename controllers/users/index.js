//gracias a este archivo solo importamos al archivo server un archivo y no todos los de las funciones controladoras

const newUser = require('./newUser');
const validateUser = require('./validateUser');
const loginUser = require('./loginUser');
const getUser = require('./getUser');
const editUser = require('./editUser');
const editUserPass = require('./editUserPass');
const editUserAvatar = require('./editUserAvatar');
const recoverUserPass = require('./recoverUserPass');
const resetUserPass = require('./resetUserPass');
const deleteUser = require('./deleteUser');


module.exports = {
    newUser,
    validateUser,
    loginUser,
    getUser,
    editUser,
    editUserPass,
    editUserAvatar,
    recoverUserPass,
    resetUserPass,
    deleteUser
    
    
};