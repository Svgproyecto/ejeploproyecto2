//Al momento de eliminar usuarios tenemos 2 opciones,
// la opcion de borrar el usuario, de esta manera eliminariamos todo lo referente al usuario, deberiamos tener en la base de datos el " ON DELETE CASCADE " que debe ponerse en todas las claves foraneas que quisieramos que se borrasen el borrar el id del usuario
// la opcion de anonimizarlo: borrar los datos claves del usuario para que no actuen como usuario activo en la web, pero dejando en este caso los comentarios que a dejado por la web(en la foto avatar podria quedarse un interrogante o un anonimo, lo que queramos vaya)
//aqui vamos a anonimizarlo

const getDB = require('../../database/getDB');

//cuando borramos un usurio tambien debemos eliminar su avatar, por lo que vamos a importar la funcion que ya habias creado enargada de hacer eso
//y el generateRandomString porque vamos a anonimizar el usuario y necesitaremos una pasword falsa, por seguridad es mejor que dejarlo null, pero nisiquiera la encriptamos.
const {deletePhoto, generateRandomString} = require('../../helpers')

const deleteUser = async(req, res, next) => {
    let connection;

    try {
        connection = await getDB()

        //obtenemso el id del usuario que queremos anonimizar
        const {idUser} = req.params;

        //lanzamos un error si el id del usuario que queremos anonimizar es el del usuario principal(admin, que por norma general sera el id 1)
        if(Number(idUser) === 1) {
            const error = new Error('EL administrador no puede ser eliminado');
            error.httpStatus = 403;
            throw error;
        }

        //Obtenemos el avatar del usuario para poder pasarselo a la funcion deletePhoto y borrarlo del servidor
        const [users] = await connection.query(
            `SELECT avatar FROM users WHERE id = ?`,
            [idUser]
        );

        //si el usuario tiene un avatar lo borramos del servidor
        if(users[0].avatar) {
            await deletePhoto(users[0].avatar)
        }

        //Anonimizamos el usuario
        //en este caso no anonimizamos el email, le dariamos una posibilidad de recuperar su cuenta (esto aqui no esta hecho, pero si contemplado), para eso deberiamos crear un endopoint que me permitiese recuperar mi cuenta porque tal y como esta ahora no se podria, como aqui no vamos a hacerlo es absurdo mantener el email a no ser que legalmente nos lo requiriesen, en este caso lo vamos a eliminar
        await connection.query(
            `
                UPDATE users
                SET password = ?, username = "[deleted]", email = "[deleted]", avatar = NULL, active = 0, deleted = 1, modifiedAt = ?
                WHERE id = ?
            `,
            [generateRandomString(20), new Date(), idUser]
        );

        //si todo a ido bien enviamos una respuesta de que todo a ido bien 
        res.send({
            status:'ok',
            message: 'Usuario eliminado'
        });


    } catch (error) {
        next(error)
    }finally{
        if(connection) connection.release()
    }
}

module.exports = deleteUser;