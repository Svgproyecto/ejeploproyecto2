//consegimos un aconexionlibre con la base de datos y para eso nos conectamos con la base de datos ya que vamos a crear un nuevo usuario alli 
const getDB = require('../../database/getDB');

//Requerimos la dependencia bcrypt pata .hash las password
const bcrypt = require('bcrypt');
const saltRounds = 10; //nivel de encriptacion de bcrypt, la recomendada por el creador es 10

//importamos la funcion del fichero helpers que nos va ha ayudar a crear una cadena alfanumerica como codigo de registro
const { generateRandomString, sendMail } = require('../../helpers');

//requerimos la variable del servidor para no andar escribiendola todo el rato, se encuentra en el .env
const { PUBLIC_HOST } = process.env


//funcion controladora de el endpoin /users con el metodo POST
//segiremos una estructura semejante al fichero initDB
const newUser = async(req, res, next) => {
    let connection;

    try {
        //intentamos obtener una nueva conexion 
        connection = await getDB();

        //obtenemos los campos necesarios del body con destructurin
        const {email, password} = req.body;

        //como estos dos campos son obligatorios comproamos que estan rellenados
        if(!email || !password) {
            //para poder arrojar el error correspondiente, creamos una variable que contenga el error correspodiente
            const error =  new Error('Faltan campos');
            //ya que un error no es mas que un objeto con una serie de propiedades, le add una para ubicar dentro de el objeto error el status error de nuesto error
            error.httpStatus = 400;
            //ahora arrojamos el error que se a producido pero con la propiedad de httpStatus que le hemos ahadido
            throw error;
        }

        //Generamos un codiogo de registro
        const registrationCode = generateRandomString(40);

        //Haseamos la password
        const hashedPasswors = await bcrypt.hash(password, saltRounds);

        //Guardamos el usuario en la base de datos
        //vamos a usar las interrogaciones para que de ninguna de las maneras si nos intentan inyectan codigo malicioso puedan, ya que ninguna de las variables del array pueden contener codigo mysql, daria error. 
        await connection.query(
            `INSERT INTO users (email, password, registrationCode, createdAt) VALUES (?, ?, ?, ?)`,
            [email, hashedPasswors, registrationCode, new Date()] //con el metodo de las interrogaciones la fecha se formatea sola
        );

        //Mensaje que enviaremso al correo del usuario
        const emailBody = `
            Te acabas de registrar en ejemplo Proyecto 2.
            Pulsa este link para verificar tu cuenta: ${PUBLIC_HOST}/users/validate/${registrationCode}    
        `;

        //Enviamos el email
        await sendMail({
            to: email,
            subject: 'Activa tu usuario de ejemplo proyecto 2',
            body: emailBody
        });

        //express asume que sino ponemos el res.status('codigo de peticion') el codigo es 200, por lo que en este caso que es para si todo va bien , no haria falta ponerlo, si lo ponemos tampoco pasa nada
         res.send({
             status:'ok', 
             message: 'Usuario registrado, comprueba tu email para activarlo'
         });

    } catch (error) {
        console.error(error);//impresion de error solo para que se ubique el desarrollador
        //este next, nos lleva directos al sigiente middleware en el que podamos entrar (en este caso en el archivo server al que importamos esta funcion )
        next(error)
    }finally{
        //si existe una conexion abierta la cerramso 
        if(connection) connection.release();
    }
    
};

//estas funcino es de endopoins las exporaremos a un fichero encargado de conectar las funciones de los endpoinso controladoras con el fichero encargado de iniciar y mantener el servidor, este fichero en este caso lo llamaremos index.js
module.exports = newUser;