const getDB = require('../../database/getDB');

//importamos las funciones que hemos creado en el fichero helpers 
const { savePhoto, deletePhoto } = require('../../helpers');


const editUserAvatar = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //Obtenemos el id del usuario que queremos editar
        const {idUser} = req.params;

        //Si la propiedad "req.files.avatar" no existe lanzamos un error. Dado que esta propiedad podria no existir tenemso que comprobar previamente si existe antes de comprobar "req.files.avatar"

        //Para enviar archivos de cualquier tipo, necesitamos utilizar en el body un formato especias, (form-data), y por defecto express no puede leer ese formato, por lo que necesitamos instalar una dependecia que nos ayude(express-fileupload)
        //y creamos un middlewaer que invocamos en el fichero server casi arriba del todo que nos 'deserealize' los bodys en formato format-data
        //no podemos comprobar si unicamente no existe req.files.avatar, tambien debemos comprobar si no existe req.files, porque si no existiese esta ultima node me saltaria un error al intentar leer el segundo campo de un undefined, para eso podemos evitarlo como david(dejo comentado), o con l ainterrogacion que nos mostro anxo.
        //asi comprobamos si no existe el avatar
        /* 
        if(!(req.files && req.files.avatar)){
            ...
        } 
        */
       if(!req.files?.avatar) {
           const error = new Error('Faltan campos');
           error.httpStatus = 400;
           throw error;
       }

       //ya comprobamos que el usuario exista por que lo vamos a comprobar con el middleware de user exists

       //Obtenemos el avatar(nombre del avatar en realidad) anteriro del usuario para asi eliminarlo y no almacenar avatares que no se estan utilizando en la  base de datos 
       //cuando elimianmos una foto del servidor la eliminamos del servidor y luego de la base de datos  y tiene que ser por ese orden porque si lo hacemso al reves no vamos a saber con que nombre hemos guardado la foto en el servidor ya no podemos borrarla 
       const [users] = await connection.query(
           `SELECT avatar FROM users WHERE id = ?`,
           [idUser]
       );
        //esta query anterior, nos va a delolver un usuario que va a tener el la posicion 0 un avatar, y vamos a generer con una dependencia un codigo alfanumerico para referirnos a el (users[0].avatar = dsjkgh qu4y23h.jpg ) , ESTO VA A OCURRIR EN EL FICHER HELPERS 

        //Comprobamos si el usuario que intentamos editar ya tiene avatar
        if(users[0].avatar){
            //Eliminamos el avatar del servidor 
            await deletePhoto(users[0].avatar)
        }

        //Guardamos el nuevo avatar en el servidor y antes de guardarlo generamos automaticamente un nombre y lo obtenemos.
        //para guardar las imagenes siempre vamos a seguir dos pasos, guardarlas en una carpeta en el servidor, y guardarlas en la base de datos.
        const avatarName = await savePhoto(req.files.avatar, 0);

        //Actualizamos el usuario con el nombre del nuevo avatar
        await connection.query(
            `UPDATE users SET avatar = ?, modifiedAt = ? WHERE id = ?`,
            [avatarName, new Date(), idUser]
        );

        res.send({
            status:'ok',
            message:'Usuario actualizado', 

        })

    } catch (error) {
        next(error);
    }finally{
        if(connection) connection.release()
    }
}

module.exports = editUserAvatar;