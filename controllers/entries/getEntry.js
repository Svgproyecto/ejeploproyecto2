const getDB = require('../../database/getDB');

const getEntry = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //obtenemos el id de la entrada que queremos obtener 
        //en vez de comprobar si existe aqui, como vamos a hacerlo muchas veces, vamos a crear un middleware que llamaremos en el fichero server.js antes que a esta funcion, y de esta manera comprobaremos si existe o no la entrada a la que intentamos hacer referencia 
        const {idEntry} = req.params

    /* 
        //Obtenemos la informacion de la entrada de la base de datos, sin los votos incorporados con left join
        const [entries] = await connection.query(
            `SELECT id, title, description, createdAt, idUser FROM entries WHERE id = ?`,
            [idEntry]
        );
    */

        //Obtenemos la informacion de la entrada de la base de datos y add los datos de los votos con left join
        //como vamos a trabajar con dos tablas devemos especificar de que tabla es cada cosa (entries.tal)
        const [entries] = await connection.query(
            `SELECT entries.id, entries.title, entries.description, entries.createdAt, entries.idUser, AVG(IFNULL(entry_votes.vote, 0)) AS votes
            FROM entries 
            LEFT JOIN entry_votes ON (entries.id = entry_votes.idEntry)
            WHERE entries.id = ?`,
            [idEntry]
        );
    /*  
        //intento fallido de hacerlo sin el left join como las photos
        //Obtenemos la media de votos y la vamos a incorporar a la tabla de entries con u left join (dejo comentada la version sin lso votos)
        const [votes] =  await connection.query(
            `SELECT AVG(IFNULL(vote, 0)) AS votes FROM entry_votes WHERE idEntry = ?`, //para hayar la media, utilizamos esta query avg. y el if null hace que si la variable vote es 0 coja por defecto el valor 0, ademas al hayar la media, esto genera una nueva columna, por lo que debemos darle nombre con AS (nombre de la nueva columna)
            [idEntry]
        )
    */

        //obtenemos las fotos de la entrada
        const [photos] = await connection.query(
            `SELECT name FROM entry_photos WHERE idEntry = ?`,
            [idEntry]
        );
        
        //Enviamos la respuesta, y utilizamos el spring operator[...arr], para incorporar las fotos a las entradas, tambien se podria hacer una query de mysql mas compleja para add a la base de datos
        res.send({
            status:'ok', 
            data: {
                entry: {
                    ...entries[0], 
                    photos, 
                    //votes: votes[0].votes
                }
            }
        })

    } catch (error) {
        next(error)
    }finally{
        if(connection) connection.release()
    }
}

module.exports = getEntry