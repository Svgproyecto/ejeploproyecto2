const getDB = require('../../database/getDB');
const { savePhoto } = require('../../helpers');

const newEntry = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        //obtenemos las propiedades del body que requiramos 
        const {title, description} = req.body;

        //Obtenemso el id del usuario que intenta crear la enrtada
        //para obtener el id del usuario lo cogeremos del token, 
        //nosotros en el middleware de isAuth hemos creado una propiedad 'req.userAuth' que contiene el id y el role del usuario 
        //este valor resta disponible aqui porque el middleware de isAuth va por delante de este archivo y lleva el next() que le pasa el control
        const idReqUser = req.userAuth.id; 
        
        //comprobamos que viene los campos requeridos en el body, y si falta algun campo lanzamos un error 
        if(!(title || description)){
            const error = new Error('Faltan campos');
            error.httpStatus = 400;
            throw error;
        }

        //si todo a ido bien creamos la entrada y obtenemso el valor de retornar "connection.query" ya que me va a hacer falta obtener el id de la entrada que acabo de crear porque tambien le dejamos subir 3 fotos a esa entrada 
        //generalmente cuadno hacemos un new Insert, no guardamso lo que devuelve connection.query pero como en este caso vamos a necesitar hacer un select pues si los guardamos 
        //cuando hacemos un consulta de un connection.query de tipo insert, update o delete, lo que nos devuleve no es el arrays de arrays que se nos devolvia al hacerlo de un select, sino que lo que se nos devuelve es un array con un objeto dentro
        //a nosotros nos interesa la propiedad inserId , que nos devuelve el id de esa 'peticion'
        //asi obtendremos el id de la entrada que acabamos de crear (esto como hemos mencionado es para poder insertar varias fotos si las ubiese)
        const [newEntry] = await connection.query(
            `INSERT INTO entries (title, description, idUser, createdAt) VALUES (?, ?, ?, ?)`,
            [title, description, idReqUser, new Date()]
        );

        //obtenemso el id de la entrada que acabamso de crear 
        const idEntry = newEntry.insertId

        //como permitimos subir hasta 3 fotos, comprobamos que 'req.files' exista y si tiene una o mas archivos(fotos)
        //Object.keys(req.files) --> ['namePhoto1', 'namePhoto2', 'namePhoto3']
        //Object.keys(req.files) --> [{objeto con propiedades de la namePhoto1}, {...}, {...}]
        //asi comprobamos si existe algun afoto en la enrtada, si hay alguna entrada entrariamos en este if
        if(req.files && Object.keys(req.files).length > 0){
            //Recorremos los valores de 'req.files'. Por si acaso nos quedamos solo con los 3 promeros valores de la posicion del array, esto solo es por temas de seguridad
            for(const photo of Object.values(req.files).slice(0, 3)){
               //Guardamos la foto en el servidor y obtenemso su nombre
               const photoName = await savePhoto(photo, 1);

               //Guardamos la foto en la tabla de fotos 
               await connection.query(
                   `INSERT INTO entry_photos (name, idEntry, createdAt) VALUES (?, ?, ?)`,
                   [photoName, idEntry, new Date()]
               )
            }


        }

        res.send({
            status:'ok', 
            message:'La entrada a sido creada'
        })


    } catch (error) {
        next(error)
    }finally{
        if(connection) connection.release()
    }
}

module.exports = newEntry